# Rust Axum Question and Answer Server - Frontend

This yew based rust frontend is derived from [knock-knock-yew](https://github.com/pdx-cs-rust-web/knock-knock-yew/tree/main) authored by Bart Massey

## Contents 

I'll do my best to keep these contents up-to-date as changes are made to the source code

* Setup
* Dependency overview
* Source overview
* Looking ahead


#### Build and Run
```
cd frontend
cargo build
trunk serve
```

Then, access the webserver shown in standard output in a browser of your choice

### Dependency overview

#### std

#### [Yew](https://crates.io/crates/yew)
Frontend rendering engine and framework to allow the combination of state and rendering into re-usable components. Similar to [ReactJS](https://react.dev/) with which I am familiar

#### [gloo](https://crates.io/crates/gloo)
Assists with error handling in the wasm content

#### [Serde/serde_json](https://crates.io/crates/serde)
A useful package for serializing and deserializing data. 

#### [wasm_bindgen_futures](https://crates.io/crates/wasm_bindgen_futures)
Allows the connection of Rust futures to behave like javascript promises in a wasm content


### Source overview

#### [`src/main.rs`](/frontend/src/main.rs)
This is primariliy the implementation of the Yew component for the `App`, that being the top level wrapper for what this page should render in the DOM. It shows us the finder and question, and also handles state managemnet.

#### [`src/finder.rs`](/frontend/src/finder.rs)
Finder is a wrapper component containing a search bar and find button, which allows the user to find a question by a given id. This component deals with mainting state and binding the button to a callback function. 

#### [`src/question.rs`](/frontend/src/question.rs)
The question component renders the question by showing its parts, that being title, content, tags and id. It is also responsible for fetching the data when passed an id by props

### Looking ahead
* It would be nice to build out more functionality, specifically at least some function to utilize each API on the backend. Seperate components to help create, update and delete questions could be made, or the question components currently there could be modified to allow mutation, deletion and creation.
* Style - A little style goes a long way to improve the user experience. That said, I prefer occasions when a UI designer provides me a style guide or mock up.
