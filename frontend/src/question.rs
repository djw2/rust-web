use crate::*;

#[derive(Properties, Clone, PartialEq, serde::Deserialize, Debug)]
pub struct QuestionStruct {
    pub id: i32,
    pub title: String,
    pub content: String,
    pub tags: Option<HashSet<String>>,
    pub source: Option<String>,
}

impl QuestionStruct {
    pub async fn get_question(key: Option<String>) -> Msg {
        let request = match &key {
            None => "http://localhost:3000/api/v1/question/0".to_string(), // on the backend, 0 = random
            Some(ref key) => format!("http://localhost:3000/api/v1/question/{}", key,),
        };
        let response = http::Request::get(&request).send().await;
        match response {
            Err(e) => Msg::GotQuestion(Err(e)),
            Ok(data) => Msg::GotQuestion(data.json().await),
        }
    }
}
pub fn format_tags(tags: &HashSet<String>) -> String {
    let taglist: Vec<&str> = tags.iter().map(String::as_ref).collect();
    taglist.join(", ")
}

#[derive(Properties, Clone, PartialEq, serde::Deserialize)]
pub struct QuestionProps {
    pub question: QuestionStruct,
}

#[function_component(Question)]
pub fn question(question: &QuestionProps) -> Html {
    let question = &question.question;
    html! { <>
        <div class="questions-container">
            <p class="title">{format!("{}:",question.title.clone())}</p>
            <p class="question">{question.content.clone()}</p>
        </div>
        <span class="annotation">
            {format!("[id: {}", &question.id)}
            if let Some(ref tags) = question.tags {
                {format!("; tags: {}", &format_tags(tags))}
            }
            if let Some(ref source) = question.source {
                {format!("; source: {}", source)}
            }
            {"]"}
        </span>
    </> }
}
