-- Create questions table
CREATE TABLE IF NOT EXISTS questions (
    id SERIAL PRIMARY KEY,
    title TEXT NOT NULL,
    content TEXT NOT NULL
);

-- Create tags table
CREATE TABLE IF NOT EXISTS tags (
    id SERIAL PRIMARY KEY,
    label TEXT UNIQUE NOT NULL
);

-- Create question_tag table
CREATE TABLE IF NOT EXISTS question_tag (
    question_id INT REFERENCES questions(id),
    tag_id INT REFERENCES tags(id),
    PRIMARY KEY (question_id, tag_id),
    FOREIGN KEY (question_id) REFERENCES questions(id),
    FOREIGN KEY (tag_id) REFERENCES tags(id)
);

-- Create answers table
CREATE TABLE IF NOT EXISTS answers (
    id SERIAL PRIMARY KEY,
    question_id INT REFERENCES questions(id),
    FOREIGN KEY (question_id) REFERENCES questions(id),
    content TEXT NOT NULL
);
