use tag::Tag;

/// Contains struct definitions regarding questions
use crate::*;

#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct NewQuestion {
    pub title: String,
    pub content: String,
    pub tags: Vec<String>,
}

#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct QuestionDTO {
    pub id: u8,
    pub title: String,
    pub content: String,
    pub tags: Vec<String>,
}
impl QuestionDTO {
    pub fn new(question: Question, tags: Vec<Tag>) -> Self {
        QuestionDTO {
            id: question.id,
            title: question.title,
            content: question.content,
            tags: tags.iter().map(|tag| tag.label.clone()).collect(),
        }
    }
}
impl IntoResponse for &QuestionDTO {
    fn into_response(self) -> Response {
        (StatusCode::OK, Json(&self)).into_response()
    }
}

#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct Question {
    pub id: u8,
    pub title: String,
    pub content: String,
}

impl Question {
    pub fn new(id: u8, title: String, content: String) -> Self {
        Question { id, title, content }
    }
}
impl IntoResponse for &Question {
    fn into_response(self) -> Response {
        (StatusCode::OK, Json(&self)).into_response()
    }
}
