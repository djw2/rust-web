/// Contains struct definitions regarding answers
use crate::*;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct NewAnswer {
    pub content: String,
    pub question_id: u8,
}

impl NewAnswer {
    pub fn _new(content: String, question_id: u8) -> Self {
        NewAnswer {
            content,
            question_id,
        }
    }
}

#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct Answer {
    id: u8,
    content: String,
    question_id: u8,
}

/// Answer Data Transfer Object, a representation of the expected serialized JSON formated of answer regarding requests, responses, and our answer json file
impl Answer {
    pub fn new(id: u8, content: String, question_id: u8) -> Self {
        Answer {
            id,
            content,
            question_id,
        }
    }
}
impl IntoResponse for &Answer {
    fn into_response(self) -> Response {
        (StatusCode::OK, Json(&self)).into_response()
    }
}
