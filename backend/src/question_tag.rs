use crate::*;

#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct QuestionTag {
    pub question_id: u8,
    pub tag_id: u8,
}

impl QuestionTag {
    pub fn new(question_id: u8, tag_id: u8) -> Self {
        QuestionTag {
            question_id,
            tag_id,
        }
    }
}
