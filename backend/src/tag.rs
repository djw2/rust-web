use crate::*;

#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct Tag {
    pub id: u8,
    pub label: String,
}

impl Tag {
    pub fn new(id: u8, label: String) -> Self {
        Tag { id, label }
    }
}
