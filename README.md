###### David Westgate
# Rust Question and Answer Web app

## Background
This is a simple [Rust](https://www.rust-lang.org/) Web [Axum](https://docs.rs/axum/latest/axum/) server, so far serving a handful of [REST](https://en.wikipedia.org/wiki/REST) [API](https://en.wikipedia.org/wiki/API) endpoints as the basis of a "Q&A" or question and answer application. The APIs supported offer basic [CRUD](https://www.codecademy.com/article/what-is-crud) functionality. Specifically, this example mirrors that which is found in the early chapters of the [Rust Web Development textbook by Bastian Gruber](https://rustwebdevelopment.com/) (except using Axum and not Warp). A frontend web server using [Yew](https://crates.io/crates/yew) is also provided to utilize the APIs

### Setup

#### Rustup
First install Rust. I suggest using the [rustup toolchain](https://rustup.rs/)

Then run 
```bash
cargo install trunk
rustup target install wasm32-unknown-unknown
```

Finally, run both the backend and frontend to use the application

### Using the backend crate

See [backend/README.md](./backend/README.md)

### Using the frontend crate

See [frontend/README.md](./frontend/README.md)

### State of things

This application is a proof of concenpt of the ability to use Rust for full stack web developmnet as well as an exercise in my experience using rust. To assist with running/developing this application, here is a look at my enviornment while I am working

![dev](./dev.png)

#### From Top left, counterclockwise
1) Terminal in `./frontend` running `trunk serve`
2) Terminal in `./backend` running `cargo run`
3) `postgres:latest` docker container with an `exec` terminal. In the terminal, I run `psql -U postgres` to connect to postgres, then `\dt` to confirm the tables exist. From there, any query can be written or tested.
4) Postman, to test the APIs for which there is not a frontend compoment.
5) Browsers connected to `http://127.0.0.1:8080` to use the web app.

